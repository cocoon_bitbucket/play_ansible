play ansible 
============



docker run -ti -v /var/run/docker.sock:/var/run/docker.sock --volumes-from dockertools_data_1 -v /tmp:/tmp cocoon/dockertools




Générez clefs privée/publique si vous n’en avez pas déjà :
$ ssh-keygen

Copiez la clef publique sur le serveur cible (qui sera désigné par 192.168.1.1 dans ce tutoriel, mais bien entendu remplacez par l’adresse de votre serveur cible).
$ ssh-copy-id -i ~/.ssh/id_rsa.pub root@192.168.1.1

Créez le fichier /etc/ansible/hosts qui contiendra la liste des serveurs à gérer, et placez-y l’adresse de votre serveur:
$ sudo vim /etc/ansible/hosts
192.168.1.1

Testez que le serveur soit bien accessible:
$ ansible all -m ping -u root 
devrait retourner:
192.168.1.1 | success >> {
"changed": false,
"ping": "pong"
}

Bravo, Ansible est installé et peut communiquer avec votre serveur cible. En avant pour la magie !

